# -*- coding: utf-8 -*-
"""
Created on Wed Sep 18 03:02:15 2019

@author: UntamedYeti
"""

from bs4 import BeautifulSoup
import requests
import os
from tqdm import tqdm
import math

def downloadme( website, my_folder ): #takes a website and target folder paramater
    if not os.path.exists(my_folder):
        os.makedirs(my_folder)
    os.chdir(my_folder)
    r  = requests.get(website)
    data = r.text
    soup = BeautifulSoup(data, "lxml")
    #loop through website to get valid links and download them to their respective folders
    dl = website + r'/'
    for link in soup.find_all('a'):
        link.get('href')
        if link.get('href') != None:
            if link.get('href').endswith(".zip") or link.get('href').endswith(".7z") or link.get('href').endswith(".rar") or link.get('href').endswith(".wad") or link.get('href').endswith(".iso"):
                print (dl+link.get('href'))
                data = dl+link.get('href')
                if os.path.isfile(link.get('href')) == False: # if false grab file if True we are skipping
                    try:
                        #wget.download(data)  # data is the url <--this did not download file
                        r = requests.get(data, stream = True, timeout=(30, 30)) #timeout is connect, read
                        # Total size in bytes.
                        total_size = int(r.headers.get('content-length', 0)); 
                        block_size = 2048
                        wrote = 0 
    
                        with open (link.get('href'), 'wb') as f:
                            for dataFile in tqdm(r.iter_content(block_size), total=math.ceil(total_size//block_size) , unit='KB', unit_scale=True):
                                wrote = wrote  + len(dataFile)
                                f.write(dataFile)
                        if total_size != 0 and wrote != total_size:
                            print("ERROR, something went wrong")
                            try:
                                os.remove(link.get('href')) # remove bad file
                            except:
                                next
                    except:
                        next
                        print('failed')
                        try:
                            os.remove(link.get('href')) # remove bad file
                        except:
                            next
                else:
                    print ('Already have file')
    return


os.chdir(r'C:\download') #this is the base folder you want to install


#mame
downloadme(r'targetIndexAddress', 'newfolder') #pass website and target folder

print('done')
